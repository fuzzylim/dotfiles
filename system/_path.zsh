export PATH="./bin:/usr/local/bin:/usr/local/sbin:$ZSH/bin:$PATH:/Users/kelvinlim/.composer/vendor/bin:/Users/kelvinlim/.composer/vendor/laravel/spark-installer:/usr/local/opt/gnu-sed/libexec/gnubin:./vendor/bin:$PATH"
export MANPATH="/usr/local/man:/usr/local/mysql/man:/usr/local/git/man:$MANPATH"
export NVM_DIR="$HOME/.nvm"
. "$(brew --prefix nvm)/nvm.sh"